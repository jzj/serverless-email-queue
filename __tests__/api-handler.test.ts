/* eslint-disable no-undef */
import { APIGatewayEvent, Context } from 'aws-lambda';

const testMessage = {
  from: 'foobar@example.com',
  to: ['success@simulator.amazonses.com'],
  subject: 'test #1',
  text: 'just a test',
};

const corsHeaders = {
  'Access-Control-Allow-Headers': 'Content-Type',
  'Access-Control-Allow-Methods': 'OPTIONS,POST',
  'Access-Control-Allow-Origin': '*',
};

const emptyQueueUrlResponse = {
  body: JSON.stringify(
    {
      status: 'ERROR',
      response: {
        message: 'QUEUE_URL is not set',
      },
    },
    null
  ),
  headers: corsHeaders,
  statusCode: 200,
};

const successfulSqsResponse = {
  ResponseMetadata: {
    RequestId: '6b6c1f46-2d0f-5b61-889b-47096fd1db88',
  },
  MD5OfMessageBody: 'ae15ff7ec59ef2554e7b90efeaf83b98',
  MessageId: 'cfe551c9-6699-46a8-a66a-a1b751bce2e9',
};

const okResponse = {
  status: 'OK',
  response: successfulSqsResponse,
};

const successfulResponse = {
  body: JSON.stringify(
    {
      status: 'OK',
      response: okResponse,
    },
    null
  ),
  headers: corsHeaders,
  statusCode: 200,
};

const failedResponse = {
  ...successfulResponse,
  ...{
    body: JSON.stringify(
      {
        status: 'ERROR',
        response: { error: 'unknown error'},
      },
      null
    ),
  },
};

describe('Unit tests for API Gateway Lambda function', function () {
  const OLD_ENV = process.env;

  beforeEach(() => {
    jest.resetModules(); // it clears the cache
    process.env = { ...OLD_ENV }; // make a copy
  });

  afterAll(() => {
    process.env = OLD_ENV; // restore old env
  });

  it('empty QUEUE_URL', async () => {
    const promiseMock = jest.fn().mockReturnValue(Promise.resolve(successfulSqsResponse));
    const sendMessageMock = jest.fn().mockReturnValue({ promise: promiseMock });
    const sendMessageBatchMock = jest.fn().mockReturnValue({ promise: promiseMock });
    jest.mock('aws-sdk/clients/sqs', () => {
      return function () {
        return {
          sendMessage: sendMessageMock,
        };
      };
    });
    process.env.QUEUE_URL = '';
    const api = require('../src/api-handler');
    const event = {
      body: JSON.stringify(testMessage),
    } as APIGatewayEvent;
    const context = {} as Context;
    const results = await api.main(event, context, () => {});
    expect(results).toEqual(emptyQueueUrlResponse);
    expect(sendMessageMock).not.toHaveBeenCalled();
    expect(sendMessageBatchMock).not.toHaveBeenCalled();
    sendMessageMock.mockRestore();
    sendMessageBatchMock.mockRestore();
    promiseMock.mockRestore();
  });

  it('SES.sendMessage error', async () => {
    const promiseMock = jest.fn().mockReturnValue(Promise.reject(new Error('unknown error')));
    const sendMessageMock = jest.fn().mockReturnValue({ promise: promiseMock });
    const sendMessageBatchMock = jest.fn().mockReturnValue({ promise: promiseMock });
    jest.mock('aws-sdk/clients/sqs', () => {
      return function () {
        return {
          sendMessage: sendMessageMock,
        };
      };
    });
    process.env.QUEUE_URL = 'https://sqs.us-east-1.amazonaws.com/123456789012/EmailMessageQueue';
    const event = {
      body: JSON.stringify(testMessage),
    } as APIGatewayEvent;
    const messageBody = JSON.stringify(testMessage);
    const expectedSendMessageParams = {
      MessageBody: messageBody,
      QueueUrl: process.env.QUEUE_URL,
    };
    const context = {} as Context;
    const api = require('../src/api-handler');
    const results = await api.main(event, context, () => {});
    expect(results.statusCode).toBe(200);
    expect(results.headers).toEqual(failedResponse.headers);
    expect(JSON.parse(results.body)).toEqual(JSON.parse(failedResponse.body));
    expect(sendMessageMock).toHaveBeenCalledTimes(1);
    expect(sendMessageBatchMock).not.toHaveBeenCalled();
    expect(sendMessageMock).toHaveBeenCalledWith(expectedSendMessageParams);
    sendMessageMock.mockRestore();
    sendMessageBatchMock.mockRestore();
    promiseMock.mockRestore();
  });

  it('one email', async () => {
    const promiseMock = jest.fn().mockReturnValue(Promise.resolve(successfulSqsResponse));
    const sendMessageMock = jest.fn().mockReturnValue({ promise: promiseMock });
    const sendMessageBatchMock = jest.fn().mockReturnValue({ promise: promiseMock });
    jest.mock('aws-sdk/clients/sqs', () => {
      return function () {
        return {
          sendMessage: sendMessageMock,
        };
      };
    });
    process.env.QUEUE_URL = 'https://sqs.us-east-1.amazonaws.com/123456789012/EmailMessageQueue';
    const event = {
      body: JSON.stringify(testMessage),
    } as APIGatewayEvent;
    const messageBody = JSON.stringify(testMessage);
    const expectedSendMessageParams = {
      MessageBody: messageBody,
      QueueUrl: process.env.QUEUE_URL,
    };
    const context = {} as Context;
    const api = require('../src/api-handler');
    const results = await api.main(event, context, () => {});
    expect(results.statusCode).toBe(200);
    expect(results.headers).toEqual(successfulResponse.headers);
    expect(JSON.parse(results.body)).toEqual(okResponse);
    expect(sendMessageMock).toHaveBeenCalledTimes(1);
    expect(sendMessageBatchMock).not.toHaveBeenCalled();
    expect(sendMessageMock).toHaveBeenCalledWith(expectedSendMessageParams);
    sendMessageMock.mockRestore();
    sendMessageBatchMock.mockRestore();
    promiseMock.mockRestore();
  });

  it('SES.sendMessageBatch error', async () => {
    const promiseMock = jest.fn().mockReturnValue(Promise.reject(new Error('unknown error')));
    const sendMessageMock = jest.fn().mockReturnValue({ promise: promiseMock });
    const sendMessageBatchMock = jest.fn().mockReturnValue({ promise: promiseMock });
    jest.mock('aws-sdk/clients/sqs', () => {
      return function () {
        return {
          sendMessage: sendMessageMock,
          sendMessageBatch: sendMessageBatchMock,
        };
      };
    });
    process.env.QUEUE_URL = 'https://sqs.us-east-1.amazonaws.com/123456789012/EmailMessageQueue';
    const event = {
      body: JSON.stringify([testMessage]),
    } as APIGatewayEvent;
    const messageBody = JSON.stringify(testMessage);
    const expectedSendMessageBatchParams = {
      Entries: [
        {
          Id: '1',
          MessageBody: messageBody,
        },
      ],
      QueueUrl: process.env.QUEUE_URL,
    };
    const context = {} as Context;
    const api = require('../src/api-handler');
    const results = await api.main(event, context, () => {});
    expect(results.statusCode).toBe(200);
    expect(results.headers).toEqual(failedResponse.headers);
    expect(JSON.parse(results.body)).toEqual(JSON.parse(failedResponse.body));
    expect(sendMessageMock).toHaveBeenCalledTimes(0);
    expect(sendMessageBatchMock).toHaveBeenCalledTimes(1);
    expect(sendMessageBatchMock).toHaveBeenCalledWith(expectedSendMessageBatchParams);
    sendMessageMock.mockRestore();
    sendMessageBatchMock.mockRestore();
    promiseMock.mockRestore();
  });

  it('multiple emails', async () => {
    const promiseMock = jest.fn().mockReturnValue(Promise.resolve(successfulSqsResponse));
    const sendMessageMock = jest.fn().mockReturnValue({ promise: promiseMock });
    const sendMessageBatchMock = jest.fn().mockReturnValue({ promise: promiseMock });
    jest.mock('aws-sdk/clients/sqs', () => {
      return function () {
        return {
          sendMessage: sendMessageMock,
          sendMessageBatch: sendMessageBatchMock,
        };
      };
    });
    process.env.QUEUE_URL = 'https://sqs.us-east-1.amazonaws.com/123456789012/EmailMessageQueue';
    const event = {
      body: JSON.stringify([testMessage]),
    } as APIGatewayEvent;
    const messageBody = JSON.stringify(testMessage);
    const expectedSendMessageBatchParams = {
      Entries: [
        {
          Id: '1',
          MessageBody: messageBody,
        },
      ],
      QueueUrl: process.env.QUEUE_URL,
    };
    const context = {} as Context;
    const api = require('../src/api-handler');
    const results = await api.main(event, context, () => {});
    expect(results.statusCode).toBe(200);
    expect(results.headers).toEqual(successfulResponse.headers);
    expect(JSON.parse(results.body)).toEqual({
      response: {},
      status: 'OK',
    });
    expect(sendMessageMock).toHaveBeenCalledTimes(0);
    expect(sendMessageBatchMock).toHaveBeenCalledTimes(1);
    expect(sendMessageBatchMock).toHaveBeenCalledWith(expectedSendMessageBatchParams);
    sendMessageMock.mockRestore();
    sendMessageBatchMock.mockRestore();
    promiseMock.mockRestore();
  });
});
