// Sanity checks for JavaScript dates
//
// zero indicates the first month of the year
// The getDate() method returns the day of the month
// for the specified date according to local time.

describe('Tests for date increments', function () {
  it('add minutes', () => {
    const mayDay1 = new Date(1991, 4, 1, 11, 11, 0);
    const mayDay2 = new Date(1991, 4, 1, 11, 11, 0);
    const expectedDate = new Date(1991, 4, 1, 11, 41, 0);
    const minutesToAdd = 30;
    expect(mayDay2).toEqual(mayDay1);
    const newDate = new Date(
      mayDay2.setMinutes(mayDay2.getMinutes() + minutesToAdd)
    );
    expect(newDate).toEqual(expectedDate);
    expect(mayDay2).not.toEqual(mayDay1);
  });
  it('add minutes next hour and year', () => {
    const endOfYear = new Date(1991, 11, 31, 23, 45, 0);
    const expectedDate = new Date(1992, 0, 1, 0, 44, 0);
    const minutesToAdd = 59;
    const newDate = new Date(
      endOfYear.setMinutes(endOfYear.getMinutes() + minutesToAdd)
    );
    expect(newDate).toEqual(expectedDate);
  });
  it('add days', () => {
    const endOfYear = new Date(1991, 11, 31, 11, 11, 0);
    const targetDate = new Date(1991, 11, 31, 11, 11, 0);
    expect(targetDate).toEqual(endOfYear);
    const expectedDate = new Date(1992, 1, 2, 11, 11, 0);
    const daysToAdd = 33;
    targetDate.setDate(targetDate.getDate() + daysToAdd);
    expect(targetDate).toEqual(expectedDate);
    expect(targetDate).not.toEqual(endOfYear);
  });
});
