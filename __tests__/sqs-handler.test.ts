/* eslint-disable no-undef */
class AwsMockSesResponseError extends Error {
  code: string;

  retryable: boolean;

  statusCode: number;

  time: Date;

  hostname: string;

  region: string;

  requestId: string;

  retryDelay: number;

  // eslint-disable-next-line max-params
  constructor(
    message: string,
    code: string,
    retryable: boolean,
    statusCode: number,
    time: Date,
    hostname: string,
    region: string,
    requestId: string,
    retryDelay: number
  ) {
    super(message);
    this.code = code;
    this.retryable = retryable;
    this.statusCode = statusCode;
    this.time = time;
    this.hostname = hostname;
    this.region = region;
    this.requestId = requestId;
    this.retryDelay = retryDelay;
  }
}

const rejectedMessageError = new AwsMockSesResponseError(
  // eslint-disable-next-line max-len
  'MessageRejected: Email address is not verified. The following identities failed the check in region US-EAST-1: bar@example.com',
  'MessageRejected',
  false,
  404,
  new Date('2020-12-10T20:18:07.650Z'),
  'hostname',
  'us-east-1',
  '8042cc04-ebf9-4df1-85df-94d605d2e8e1',
  87.17221253223452
);

import { Context } from 'aws-lambda';

const event = {
  Records: [
    {
      messageId: '059f36b4-87a3-44ab-83d2-661975830a7d',
      receiptHandle: 'AQEBwJnKyrHigUMZj6rYigCgxlaS3SLy0a...',
      body: '{"from":"foo@example.com","to":["bar@example.com"],"subject":"test #1","text":"just a test"}',
      attributes: {
        ApproximateReceiveCount: '1',
        SentTimestamp: '1545082649183',
        SenderId: 'AIDAIENQZJOLO23YVJ4VO',
        ApproximateFirstReceiveTimestamp: '1545082649185',
      },
      messageAttributes: {},
      md5OfBody: 'e4e68fb7bd0e697a0ae8f1bb342846b3',
      eventSource: 'aws:sqs',
      eventSourceARN: 'arn:aws:sqs:us-east-2:123456789012:my-queue',
      awsRegion: 'us-east-1',
    },
    {
      messageId: '2e1424d4-f796-459a-8184-9c92662be6da',
      receiptHandle: 'AQEBzWwaftRI0KuVm4tP+/7q1rGgNqicHq...',
      body: '{"from":"foo@example.com","to":["bar@example.com"],"subject":"test #2","text":"just a test again"}',
      attributes: {
        ApproximateReceiveCount: '1',
        SentTimestamp: '1545082650636',
        SenderId: 'AIDAIENQZJOLO23YVJ4VO',
        ApproximateFirstReceiveTimestamp: '1545082650649',
      },
      messageAttributes: {},
      md5OfBody: 'e4e68fb7bd0e697a0ae8f1bb342846b3',
      eventSource: 'aws:sqs',
      eventSourceARN: 'arn:aws:sqs:us-east-2:123456789012:my-queue',
      awsRegion: 'us-east-1',
    },
  ],
};

const eventWithInvalidMessage = {
  Records: [
    {
      messageId: '059f36b4-87a3-44ab-83d2-661975830a7d',
      receiptHandle: 'AQEBwJnKyrHigUMZj6rYigCgxlaS3SLy0a...',
      body: 'foobar',
      attributes: {
        ApproximateReceiveCount: '1',
        SentTimestamp: '1545082649183',
        SenderId: 'AIDAIENQZJOLO23YVJ4VO',
        ApproximateFirstReceiveTimestamp: '1545082649185',
      },
      messageAttributes: {},
      md5OfBody: 'e4e68fb7bd0e697a0ae8f1bb342846b3',
      eventSource: 'aws:sqs',
      eventSourceARN: 'arn:aws:sqs:us-east-2:123456789012:my-queue',
      awsRegion: 'us-east-1',
    },
  ],
};

const successfulDynamoDbResponse = {
  ConsumedCapacity: [
    {
      CapacityUnits: 10,
      TableName: 'FooBar',
    },
  ],
  UnprocessedItems: {},
};

const mockDynamoDbPut = jest.fn().mockReturnValue(Promise.resolve(successfulDynamoDbResponse));
const mockDynamoDbErrorPut = jest.fn().mockReturnValue(Promise.reject(new Error('unknown error')));
const awsSdkPromiseResponse = jest.fn().mockReturnValue(Promise.resolve(true));
const getFn = jest.fn().mockImplementation(() => ({ promise: awsSdkPromiseResponse }));
const putFn = jest.fn().mockImplementation(() => ({ promise: mockDynamoDbPut }));
const putErrorFn = jest.fn().mockImplementation(() => ({ promise: mockDynamoDbErrorPut }));
const MockDocumentClient = jest.fn().mockImplementation(() => ({
  get: getFn,
  put: putFn,
}));
const MockErrorDocumentClient = jest.fn().mockImplementation(() => ({
  get: getFn,
  put: putErrorFn,
}));

describe('Unit tests for emailer Lambda function', function () {
  const OLD_ENV = process.env;

  beforeEach(() => {
    jest.resetModules(); // most important - it clears the cache
    process.env = { ...OLD_ENV }; // make a copy
  });

  afterAll(() => {
    process.env = OLD_ENV; // restore old env
  });

  it('send two emails with no results database table', async () => {
    const sendMailMock = jest.fn().mockReturnValue(Promise.resolve(true));
    const transportMock = jest.fn().mockImplementation(() => {
      return {
        sendMail: sendMailMock,
      };
    });
    jest.mock('nodemailer', () => ({
      createTransport: transportMock,
    }));
    jest.mock('aws-sdk', () => {
      return {
        DynamoDB: {
          DocumentClient: MockDocumentClient,
        },
        SES: jest.fn(() => ({
          sendEmail: { promise: awsSdkPromiseResponse },
          sendRawEmail: { promise: awsSdkPromiseResponse },
        })),
      };
    });
    const consoleErrorSpy = jest.spyOn(global.console, 'error').mockImplementation();
    const consoleLogSpy = jest.spyOn(global.console, 'log').mockImplementation();
    const consoleInfoSpy = jest.spyOn(global.console, 'info').mockImplementation();
    const context = {} as Context;
    const emailer = require('../src/sqs-handler');
    const results = await emailer.main(event, context, () => {});
    expect(results).toBe(undefined);
    expect(sendMailMock).toHaveBeenCalled();
    expect(sendMailMock.mock.calls.length).toBe(2);
    expect(sendMailMock).toHaveBeenCalledWith(JSON.parse(event.Records[0].body));
    expect(sendMailMock).toHaveBeenCalledWith(JSON.parse(event.Records[1].body));
    expect(consoleErrorSpy).toHaveBeenCalledTimes(0);
    expect(consoleLogSpy).toHaveBeenCalledTimes(0);
    expect(consoleInfoSpy).toHaveBeenCalledTimes(0);
    expect(mockDynamoDbPut).not.toHaveBeenCalled();
    consoleErrorSpy.mockRestore();
    consoleLogSpy.mockRestore();
    consoleInfoSpy.mockRestore();
    sendMailMock.mockRestore();
    transportMock.mockRestore();
  });

  it('debug enabled', async () => {
    const sendMailMock = jest.fn().mockReturnValue(Promise.resolve(true));
    const transportMock = jest.fn().mockImplementation(() => {
      return {
        sendMail: sendMailMock,
      };
    });
    jest.mock('nodemailer', () => ({
      createTransport: transportMock,
    }));
    jest.mock('aws-sdk', () => {
      return {
        DynamoDB: {
          DocumentClient: MockDocumentClient,
        },
        SES: jest.fn(() => ({
          sendEmail: { promise: awsSdkPromiseResponse },
          sendRawEmail: { promise: awsSdkPromiseResponse },
        })),
      };
    });
    process.env.DEBUG = 'true';
    const consoleErrorSpy = jest.spyOn(global.console, 'error').mockImplementation();
    const consoleLogSpy = jest.spyOn(global.console, 'log').mockImplementation();
    const consoleInfoSpy = jest.spyOn(global.console, 'info').mockImplementation();
    const context = {} as Context;
    const emailer = require('../src/sqs-handler');
    await emailer.main(event, context, () => {});
    expect(consoleErrorSpy).toHaveBeenCalledTimes(0);
    expect(consoleLogSpy).toHaveBeenCalledTimes(3);
    expect(consoleInfoSpy).toHaveBeenCalledTimes(2);
    consoleErrorSpy.mockRestore();
    consoleLogSpy.mockRestore();
    consoleInfoSpy.mockRestore();
    sendMailMock.mockRestore();
    transportMock.mockRestore();
  });

  it('one success sent and one failed send', async () => {
    const sendMailMock = jest
      .fn()
      .mockReturnValue(Promise.resolve(true))
      .mockReturnValueOnce(Promise.resolve(true))
      .mockReturnValueOnce(Promise.reject(rejectedMessageError));
    const transportMock = jest.fn().mockImplementation(() => {
      return {
        sendMail: sendMailMock,
      };
    });
    jest.mock('nodemailer', () => ({
      createTransport: transportMock,
    }));
    jest.mock('aws-sdk', () => {
      return {
        DynamoDB: {
          DocumentClient: MockDocumentClient,
        },
        SES: jest.fn(() => ({
          sendEmail: { promise: awsSdkPromiseResponse },
          sendRawEmail: { promise: awsSdkPromiseResponse },
        })),
      };
    });
    const consoleErrorSpy = jest.spyOn(global.console, 'error').mockImplementation();
    const consoleLogSpy = jest.spyOn(global.console, 'log').mockImplementation();
    const consoleInfoSpy = jest.spyOn(global.console, 'info').mockImplementation();
    const context = {} as Context;
    const emailer = require('../src/sqs-handler');
    const results = await emailer.main(event, context, () => {});
    expect(results).toBe(undefined);
    expect(sendMailMock.mock.calls.length).toBe(2);
    expect(sendMailMock).toHaveBeenCalledWith(JSON.parse(event.Records[0].body));
    expect(sendMailMock).toHaveBeenCalledWith(JSON.parse(event.Records[1].body));
    expect(consoleErrorSpy).toHaveBeenCalledTimes(2);
    expect(consoleLogSpy).toHaveBeenCalledTimes(0);
    expect(consoleInfoSpy).toHaveBeenCalledTimes(0);
    expect(mockDynamoDbPut).not.toHaveBeenCalled();
    consoleErrorSpy.mockRestore();
    consoleLogSpy.mockRestore();
    consoleInfoSpy.mockRestore();
    sendMailMock.mockRestore();
    transportMock.mockRestore();
  });
  it('send two emails with a results database table', async () => {
    const sendMailMock = jest.fn().mockReturnValue(Promise.resolve(true));
    const transportMock = jest.fn().mockImplementation(() => {
      return {
        sendMail: sendMailMock,
      };
    });
    jest.mock('nodemailer', () => ({
      createTransport: transportMock,
    }));
    jest.mock('aws-sdk', () => {
      return {
        DynamoDB: {
          DocumentClient: MockDocumentClient,
        },
        SES: jest.fn(() => ({
          sendEmail: { promise: awsSdkPromiseResponse },
          sendRawEmail: { promise: awsSdkPromiseResponse },
        })),
      };
    });
    const consoleErrorSpy = jest.spyOn(global.console, 'error').mockImplementation();
    const consoleLogSpy = jest.spyOn(global.console, 'log').mockImplementation();
    const consoleInfoSpy = jest.spyOn(global.console, 'info').mockImplementation();
    process.env.MESSAGE_TABLE_NAME = 'FooBar';
    const context = {} as Context;
    const emailer = require('../src/sqs-handler');
    const results = await emailer.main(event, context, () => {});
    expect(results).toBe(undefined);
    expect(sendMailMock).toHaveBeenCalled();
    expect(sendMailMock.mock.calls.length).toBe(2);
    expect(sendMailMock).toHaveBeenCalledWith(JSON.parse(event.Records[0].body));
    expect(sendMailMock).toHaveBeenCalledWith(JSON.parse(event.Records[1].body));
    expect(putFn).toHaveBeenCalledTimes(2);
    expect(Object.keys(putFn.mock.calls[0][0]).sort()).toEqual(['Item', 'TableName'].sort());
    expect(putFn.mock.calls[0][0]['Item']['message']).toEqual(JSON.parse(event.Records[0].body));
    expect(putFn.mock.calls[1][0]['Item']['message']).toEqual(JSON.parse(event.Records[1].body));
    expect(consoleErrorSpy).toHaveBeenCalledTimes(0);
    expect(consoleLogSpy).toHaveBeenCalledTimes(0);
    expect(consoleInfoSpy).toHaveBeenCalledTimes(0);
    // eslint-disable-next-line no-prototype-builtins
    expect(putFn.mock.calls[1][0]['Item'].hasOwnProperty('error')).toBeFalsy();
    consoleErrorSpy.mockRestore();
    consoleLogSpy.mockRestore();
    consoleInfoSpy.mockRestore();
    sendMailMock.mockRestore();
    transportMock.mockRestore();
  });

  it('send two emails with a database error', async () => {
    const sendMailMock = jest.fn().mockReturnValue(Promise.resolve(true));
    const transportMock = jest.fn().mockImplementation(() => {
      return {
        sendMail: sendMailMock,
      };
    });
    jest.mock('nodemailer', () => ({
      createTransport: transportMock,
    }));
    jest.mock('aws-sdk', () => {
      return {
        DynamoDB: {
          DocumentClient: MockErrorDocumentClient,
        },
        SES: jest.fn(() => ({
          sendEmail: { promise: awsSdkPromiseResponse },
          sendRawEmail: { promise: awsSdkPromiseResponse },
        })),
      };
    });
    process.env.MESSAGE_TABLE_NAME = 'FooBar';
    const context = {} as Context;
    const emailer = require('../src/sqs-handler');
    const results = await emailer.main(event, context, () => {});
    expect(results).toBe(undefined);
    expect(sendMailMock).toHaveBeenCalled();
    expect(sendMailMock.mock.calls.length).toBe(2);
    expect(sendMailMock).toHaveBeenCalledWith(JSON.parse(event.Records[0].body));
    expect(sendMailMock).toHaveBeenCalledWith(JSON.parse(event.Records[1].body));
    expect(putFn).toHaveBeenCalledTimes(2);
    expect(Object.keys(putFn.mock.calls[0][0]).sort()).toEqual(['Item', 'TableName'].sort());
    expect(putFn.mock.calls[0][0]['Item']['message']).toEqual(JSON.parse(event.Records[0].body));
    expect(putFn.mock.calls[1][0]['Item']['message']).toEqual(JSON.parse(event.Records[1].body));
    // eslint-disable-next-line no-prototype-builtins
    expect(putFn.mock.calls[1][0]['Item'].hasOwnProperty('error')).toBeFalsy();
    sendMailMock.mockRestore();
    transportMock.mockRestore();
  });

  it('one success sent and one failed send with a results database table', async () => {
    const sendMailMock = jest
      .fn()
      .mockReturnValue(Promise.resolve(true))
      .mockReturnValueOnce(Promise.resolve(true))
      .mockReturnValueOnce(Promise.reject(rejectedMessageError));
    const transportMock = jest.fn().mockImplementation(() => {
      return {
        sendMail: sendMailMock,
      };
    });
    jest.mock('nodemailer', () => ({
      createTransport: transportMock,
    }));
    jest.mock('aws-sdk', () => {
      return {
        DynamoDB: {
          DocumentClient: MockDocumentClient,
        },
        SES: jest.fn(() => ({
          sendEmail: { promise: awsSdkPromiseResponse },
          sendRawEmail: { promise: awsSdkPromiseResponse },
        })),
      };
    });
    const consoleErrorSpy = jest.spyOn(global.console, 'error').mockImplementation();
    const consoleLogSpy = jest.spyOn(global.console, 'log').mockImplementation();
    const consoleInfoSpy = jest.spyOn(global.console, 'info').mockImplementation();
    process.env.MESSAGE_TABLE_NAME = 'FooBar';
    const context = {} as Context;
    const emailer = require('../src/sqs-handler');
    const results = await emailer.main(event, context, () => {});
    expect(results).toBe(undefined);
    expect(sendMailMock).toHaveBeenCalled();
    expect(sendMailMock.mock.calls.length).toBe(2);
    expect(sendMailMock).toHaveBeenCalledWith(JSON.parse(event.Records[0].body));
    expect(sendMailMock).toHaveBeenCalledWith(JSON.parse(event.Records[1].body));
    expect(putFn).toHaveBeenCalledTimes(4);
    expect(Object.keys(putFn.mock.calls[0][0]).sort()).toEqual(['Item', 'TableName'].sort());
    expect(putFn.mock.calls[0][0]['Item']['message']).toEqual(JSON.parse(event.Records[0].body));
    expect(putFn.mock.calls[1][0]['Item']['message']).toEqual(JSON.parse(event.Records[1].body));
    // eslint-disable-next-line no-prototype-builtins
    expect(putFn.mock.calls[2][0]['Item'].hasOwnProperty('error')).toBeFalsy();
    // eslint-disable-next-line no-prototype-builtins
    expect(putFn.mock.calls[3][0]['Item'].hasOwnProperty('error')).toBeTruthy();
    consoleErrorSpy.mockRestore();
    consoleLogSpy.mockRestore();
    consoleInfoSpy.mockRestore();
    sendMailMock.mockRestore();
    transportMock.mockRestore();
  });

  it('invalid JSON', async () => {
    const sendMailMock = jest.fn().mockReturnValue(Promise.resolve(true));
    const transportMock = jest.fn().mockImplementation(() => {
      return {
        sendMail: sendMailMock,
      };
    });
    jest.mock('nodemailer', () => ({
      createTransport: transportMock,
    }));
    jest.mock('aws-sdk', () => {
      return {
        DynamoDB: {
          DocumentClient: MockDocumentClient,
        },
        SES: jest.fn(() => ({
          sendEmail: { promise: awsSdkPromiseResponse },
          sendRawEmail: { promise: awsSdkPromiseResponse },
        })),
      };
    });
    const consoleErrorSpy = jest.spyOn(global.console, 'error').mockImplementation();
    const consoleLogSpy = jest.spyOn(global.console, 'log').mockImplementation();
    const consoleInfoSpy = jest.spyOn(global.console, 'info').mockImplementation();
    const context = {} as Context;
    const emailer = require('../src/sqs-handler');
    const results = await emailer.main(eventWithInvalidMessage, context, () => {});
    expect(results).toBe(undefined);
    expect(sendMailMock).not.toHaveBeenCalled();
    expect(consoleErrorSpy).toHaveBeenCalledTimes(1);
    expect(consoleLogSpy).toHaveBeenCalledTimes(0);
    expect(consoleInfoSpy).toHaveBeenCalledTimes(0);
    consoleErrorSpy.mockRestore();
    consoleLogSpy.mockRestore();
    consoleInfoSpy.mockRestore();
    sendMailMock.mockRestore();
    transportMock.mockRestore();
  });
});
