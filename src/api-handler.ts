import {
  APIGatewayEvent,
  APIGatewayProxyHandler,
  APIGatewayProxyResult,
} from 'aws-lambda';
import SQS = require('aws-sdk/clients/sqs');
import { SendMessageResult, SendMessageBatchResult } from 'aws-sdk/clients/sqs';

const queueUrl = process.env.QUEUE_URL ? process.env.QUEUE_URL.trim() : '';
const booleanStrings = ['true', 'yes', 'y', '1'];
const debug = process.env.DEBUG ? booleanStrings.includes(process.env.DEBUG.toLowerCase()) : false;

// AWS clients and settings
const sqs = new SQS({
  apiVersion: '2012-11-05',
});

// Global variables and functions
export interface EmailMessage {
  from: string;
  to: string[];
  cc?: string[];
  bcc?: string[];
  subject: string;
  text?: string;
  html?: string;
}

export interface ErrorResponse {
  error: any
}

// Lambda handler
export const main: APIGatewayProxyHandler = async (
  event: APIGatewayEvent
): Promise<APIGatewayProxyResult> => {
  let body:
    | {
        [key: string]: string | string[];
      }
    | {
        [key: string]: string | string[];
      }[] = {};
  let response:
    | SendMessageBatchResult
    | SendMessageResult
    | ErrorResponse
    | { [key: string]: string | string[] } = {};
  let responseStatus: string = 'ERROR';
  if (!queueUrl) {
    const errorMessage = 'QUEUE_URL is not set';
    console.error(errorMessage);
    response = {
      message: errorMessage,
    };
  } else {
    try {
      body = JSON.parse(event.body);
      let idCounter = 0;
      if (body instanceof Array) {
        const entries = body.map((message) => {
          idCounter++;
          return {
            Id: idCounter.toString(),
            MessageBody: JSON.stringify(message, null),
          };
        });
        if (debug) {
          console.log(`received ${entries.length} messages`);
        }
        const sendMessageBatchParams = {
          Entries: entries,
          QueueUrl: queueUrl,
        };
        await sqs
          .sendMessageBatch(sendMessageBatchParams)
          .promise()
          .then((data: SendMessageBatchResult) => {
            if (debug) {
              console.log(data);
            }
            responseStatus = 'OK';
          })
          .catch((error: Error) => {
            console.error(error);
            response = {
              error: error.message
            };
          });
      } else {
        const messageBody = JSON.stringify(body, null);
        const sendMessageParams = {
          MessageBody: messageBody,
          QueueUrl: queueUrl,
        };
        if (debug) {
          console.log('message received');
        }
        await sqs
          .sendMessage(sendMessageParams)
          .promise()
          .then((data: SendMessageResult) => {
            if (debug) {
              console.log(data);
            }
            responseStatus = 'OK';
            response = data;
          })
          .catch((error: Error) => {
            console.error(error);
            response = {
              error: error.message
            }
          });
      }
    } catch (error) {
      console.error('a fatal error occurred', error);
      response = {
        error: error.message
      }
    }
  }
  return {
    statusCode: 200,
    headers: {
      'Access-Control-Allow-Headers': 'Content-Type',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'OPTIONS,POST',
    },
    body: JSON.stringify({
      status: responseStatus,
      response,
    }),
  };
};
