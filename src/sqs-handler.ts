import { SQSHandler, SQSEvent, SQSRecord } from 'aws-lambda';
import * as nodemailer from 'nodemailer';
import { SES, DynamoDB } from 'aws-sdk';
import Mail = require('nodemailer/lib/mailer');

// Global customizable settings
// MESSAGE_TABLE_NAME - The DynamoDB table name for storing the results from send requests
const messageTableName = process.env.MESSAGE_TABLE_NAME ? process.env.MESSAGE_TABLE_NAME.trim() : '';
// SENDING_RATE - How many messages per second is allowed to be delivered to SES.
const sendingRate = process.env.SENDING_RATE ? Number.parseInt(process.env.SENDING_RATE, 10) : 5;
// MAX_CONNECTIONS = How many parallel connections to allow towards SES.
const maxConnections = process.env.MAX_CONNECTIONS ? Number.parseInt(process.env.MAX_CONNECTIONS, 10) : 5;
const booleanStrings = ['true', 'yes', 'y', '1'];
const debug = process.env.DEBUG ? booleanStrings.includes(process.env.DEBUG.toLowerCase()) : false;
// MOCK_MAILER - Enables mock SES for testing without sending emails.
const mockMailer = process.env.MOCK_MAILER ? booleanStrings.includes(process.env.MOCK_MAILER.toLowerCase()) : false;
// MESSAGE_EXPIRY_IN_DAYS - How long to store the record of the email.
// eslint-disable-next-line max-len,prettier/prettier
const messageExpiryInDays = process.env.MESSAGE_EXPIRY_IN_DAYS ? Number.parseInt(process.env.MESSAGE_EXPIRY_IN_DAYS, 10) : 30;

// AWS clients and settings
const ses = new SES({
  apiVersion: '2010-12-01',
});
const documentClient = new DynamoDB.DocumentClient({
  maxRetries: 3,
  sslEnabled: true,
  apiVersion: '2012-08-10',
});

// Global variables and functions
let transporter: Mail;

if (mockMailer) {
  console.log('enabling mock mailer');
  // create transporter object for testing
  transporter = nodemailer.createTransport({
    jsonTransport: true,
  });
} else {
  // create transporter object using the SES transport
  transporter = nodemailer.createTransport({
    SES: ses,
    sendingRate,
    maxConnections,
  });
}

export const saveMessageRecord = (messageRecord: any): Promise<boolean> => {
  const dbParams = {
    TableName: messageTableName,
    Item: messageRecord,
  };
  return documentClient
    .put(dbParams)
    .promise()
    .then(() => true)
    .catch((err: Error) => {
      console.error('db update failed', err);
      return false;
    });
};

// Lambda handler
export const main: SQSHandler = async (event: SQSEvent) => {
  if (debug) {
    console.log(`received ${event.Records.length} records`);
  }
  const sendRequests: Array<Promise<boolean>> = [];
  let numberOfSuccesses = 0;
  let numberOfFailures = 0;
  event.Records.forEach((record: SQSRecord) => {
    const { body } = record;
    try {
      const message = JSON.parse(body);
      if (debug) {
        console.log(message);
      }
      sendRequests.push(
        transporter
          .sendMail(message)
          .then(async (response) => {
            if (messageTableName) {
              const currentDate = new Date();
              const expiryDate = new Date();
              expiryDate.setDate(expiryDate.getDate() + messageExpiryInDays);
              const expiry = Math.round(expiryDate.getTime() / 1000);
              if (response && Object.prototype.hasOwnProperty.call(response, 'raw')) {
                // the "raw" data is not human readable or useful
                delete response.raw;
              }
              const messageRecord = {
                id: record.md5OfBody,
                message,
                updateAt: currentDate.toISOString(),
                expiry: expiry,
                status: 'success',
                response,
              };
              await saveMessageRecord(messageRecord);
            }
            if (mockMailer) {
              // simulate latency for testing purposes
              const pauseInMilliseconds = Math.round(Math.random() * 1000);
              await new Promise((resolve) => setTimeout(resolve, pauseInMilliseconds));
            }
            return true;
          })
          .catch(async (err: any) => {
            console.error(`${message.subject} failed`);
            console.error(err);
            if (messageTableName) {
              const { code, time, requestId, statusCode, retryable, retryDelay } = err;
              const currentDate = new Date();
              const expiryDate = new Date();
              expiryDate.setDate(expiryDate.getDate() + messageExpiryInDays);
              const expiry = Math.round(expiryDate.getTime() / 1000);
              const messageRecord = {
                id: record.md5OfBody,
                message,
                error: {
                  code,
                  time: time.toISOString(),
                  requestId,
                  statusCode,
                  retryable,
                  retryDelay,
                  message: err.message,
                },
                updateAt: currentDate.toISOString(),
                expiry: expiry,
                status: 'fail',
              };
              await saveMessageRecord(messageRecord);
            }
            return false;
          })
      );
    } catch (err) {
      console.error('a fatal error occurred', err);
    }
  });
  // Wait until all emails are processed
  await Promise.all(sendRequests)
    .then((responseStatuses) => {
      responseStatuses.forEach((wasSuccessful) => {
        // eslint-disable-next-line no-unused-expressions
        wasSuccessful ? numberOfSuccesses++ : numberOfFailures++;
      });
      if (debug) {
        console.info(`number of successes = ${numberOfSuccesses}`);
        console.info(`number of failures = ${numberOfFailures}`);
      }
    })
    .catch((err) => {
      console.error('fatal error processing send request', err);
    });
};
