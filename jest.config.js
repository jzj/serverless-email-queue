module.exports = {
  preset: 'ts-jest',
  clearMocks: false,
  collectCoverage: true,
  coverageDirectory: 'coverage',
  testEnvironment: 'node',
  testMatch: ['**/*.test.ts'],
  moduleFileExtensions: ['ts', 'js'],
  testPathIgnorePatterns: ['.aws-sam'],
};
