const { program } = require('commander');
program.version('1.0.0');
import { SES, SQS, DynamoDB } from 'aws-sdk';
import Table = require('cli-table');

// AWS clients and settings
const sqs = new SQS({
  apiVersion: '2012-11-05',
});
const ses = new SES({
  apiVersion: '2010-12-01',
});
const documentClient = new DynamoDB.DocumentClient({
  maxRetries: 3,
  sslEnabled: true,
  apiVersion: '2012-08-10',
});

interface EmailMessage {
  from: string;
  to: string[];
  cc?: string[];
  bcc?: string[];
  subject: string;
  text?: string;
  html?: string;
}

interface MessageError {
  retryable: boolean;
  retryDelay: number;
  code: string;
  requestId: string;
  time: string;
  message: string;
  statusCode: number;
}

interface MessageRecord {
  message: EmailMessage;
  updateAt: string;
  id: string;
  status: string;
  error: MessageError;
  expiry?: number;
}

const table = new Table({
  head: ['Message ID', 'Date Send', 'From', 'Subject', 'Status', 'Error Code', 'Error Message'],
});

const getMessages = (limit: number, tableName: string, status: string = ''): Promise<MessageRecord[]> => {
  let dbParams = {
    TableName: tableName,
    Limit: limit,
  };
  if (status) {
    console.log(`status filter used ${status}`);
    const filter = {
      ExpressionAttributeValues: {
        ':status': {
          S: status,
        },
      },
      FilterExpression: 'status = :status',
    };
    dbParams = {
      ...dbParams,
      ...{ filter },
    };
  }
  return documentClient
    .scan(dbParams)
    .promise()
    .then((data) => {
      return data.Items as MessageRecord[];
    })
    .catch((err: Error) => {
      console.error('db scan failed', err);
      return [];
    });
};

const getMessage = (messageId: string, tableName: string): Promise<null | MessageRecord> => {
  const dbParams = {
    TableName: tableName,
    Key: { id: messageId },
  };
  return documentClient
    .get(dbParams)
    .promise()
    .then((data) => {
      return data.Item;
    })
    .catch((err: Error) => {
      console.error('db get failed', err);
      return null;
    });
};

const deleteMessage = (messageId: string, tableName: string): Promise<void> => {
  const dbParams = {
    TableName: tableName,
    Key: { id: messageId },
  };
  return documentClient
    .delete(dbParams)
    .promise()
    .then(() => {
      console.info(`message ${messageId} deleted`);
    })
    .catch((err: Error) => {
      console.error('db delete failed', err);
    });
};

program
  .command('list-messages <tableName> [limit]', {
    isDefault: true,
  })
  .description('List the messages in a DynamoDB table.', {
    tableName: 'The DynamoDB table.',
    limit: 'The number of messages to list. 100 is the maximum and the default. Optional',
  })
  .option('-d,--debug', 'Enables the debugging output.')
  .option('-s,--status <status>', 'The status filter.')
  .action(async (tableName: string, limit: string, options: any) => {
    let limitAsNumber = 100;
    if (limit) {
      limitAsNumber = Number.parseInt(limit, 10);
    }
    if (limitAsNumber > 100 || limitAsNumber < 1) {
      limitAsNumber = 100;
    }
    const messageRows = await getMessages(limitAsNumber, tableName, options.status);
    let total = messageRows.length;
    let statsSummary: any = {};
    let numberOfFailMessages = 0;
    let numberOfSuccessMessages = 0;
    messageRows.forEach((messageRow) => {
      let from = '';
      let subject = '';
      const { error, status, message } = messageRow;
      if (message) {
        from = message.from;
        subject = message.subject;
      }
      if (status === 'fail') {
        numberOfFailMessages++;
      } else {
        numberOfSuccessMessages++;
      }
      let errorCode = '';
      let errorMessage = '';
      if (error) {
        errorCode = error.code;
        errorMessage = error.message;
      }
      table.push([messageRow.id, messageRow.updateAt, from, subject, status, errorCode, errorMessage]);
      if (errorCode) {
        if (Object.prototype.hasOwnProperty.call(statsSummary, errorCode)) {
          let count: number = Number.parseInt(statsSummary[errorCode], 10);
          count++;
          statsSummary = {
            ...statsSummary,
            ...{ [errorCode]: count },
          };
        } else {
          statsSummary = {
            ...statsSummary,
            ...{ [errorCode]: 1 },
          };
        }
      }
    });
    console.log(table.toString());
    if (options.debug) {
      console.debug(`statsSummary: ${JSON.stringify(statsSummary, null, 2)}`);
      console.debug(`fail: ${JSON.stringify(numberOfFailMessages, null, 2)}`);
      console.debug(`success: ${JSON.stringify(numberOfSuccessMessages, null, 2)}`);
      console.debug(`total: ${total}`);
    }
  });

program
  .command('send-verification <messageId> <tableName>')
  .description('Send an email verification to the sender email of a message.', {
    messageId: 'The ID of the message.',
    tableName: 'The DynamoDB table.',
  })
  .action(async (messageId: string, tableName: string) => {
    const message = await getMessage(messageId, tableName);
    if (!message) {
      console.error('message not found');
    } else {
      const sesParams = {
        EmailAddress: message.message.from,
      };
      ses
        .verifyEmailIdentity(sesParams)
        .promise()
        .then(() => {
          console.info(`verify email send to ${message.message.from}`);
        })
        .catch((err: Error) => {
          console.error('verifyEmailIdentity failed', err);
        });
    }
  });

program
  .command('get-message <messageId> <tableName>')
  .description('Get a message.', {
    messageId: 'The ID of the message.',
    tableName: 'The DynamoDB table.',
  })
  .action(async (messageId: string, tableName: string) => {
    const messageRow = await getMessage(messageId, tableName);
    if (!messageRow) {
      console.error('message not found');
    } else {
      let from = '';
      let subject = '';
      const { error, status, message } = messageRow;
      if (message) {
        from = message.from;
        subject = message.subject;
      }
      let errorCode = '';
      let errorMessage = '';
      if (error) {
        errorCode = error.code;
        errorMessage = error.message;
      }
      table.push([messageRow.id, messageRow.updateAt, from, subject, status, errorCode, errorMessage]);
      console.log(table.toString());
    }
  });

program
  .command('delete-message <messageId> <tableName>')
  .description('Delete a message.', {
    messageId: 'The ID of the message.',
    tableName: 'The DynamoDB table.',
  })
  .action(async (messageId: string, tableName: string) => {
    await deleteMessage(messageId, tableName);
  });

program
  .command('resend-message <queueUrl> <messageId> <tableName>')
  .description('Resend a failed message.', {
    queueUrl: 'The URL of the message queue.',
    messageId: 'The ID of the message.',
    tableName: 'The DynamoDB table.',
  })
  .action(async (queueUrl: string, messageId: string, tableName: string) => {
    const message = await getMessage(messageId, tableName);
    if (!message) {
      console.error('message not found');
    } else {
      const messageBody = JSON.stringify(message.message);
      const sendMessageParams = {
        MessageBody: messageBody,
        QueueUrl: queueUrl,
      };
      sqs
        .sendMessage(sendMessageParams)
        .promise()
        .then(async () => {
          console.info(`SQS sendMessage successful`);
          await deleteMessage(messageId, tableName);
        })
        .catch((err: Error) => {
          console.error('SQS sendMessage failed', err);
        });
    }
  });

program
  .command('send-test-messages <queueUrl> <from> <numberOfMessages>')
  .description('Send test messages.', {
    queueUrl: 'The URL of the message queue.',
    from: 'The from email of the message queue.',
    numberOfMessages: 'The number of messages.',
  })
  .action((queueUrl: string, from: string, numberOfMessages: string) => {
    const maxNumberOfMessages = 100;
    let numberOfMessagesToSend = Number.parseInt(numberOfMessages, 10);
    if (numberOfMessagesToSend < 1 || numberOfMessagesToSend > maxNumberOfMessages) {
      numberOfMessagesToSend = 1;
    }
    let numberOfMessagesSent = 0;
    while (numberOfMessagesSent < numberOfMessagesToSend) {
      numberOfMessagesSent++;
      const message = {
        from,
        to: ['success@simulator.amazonses.com'],
        subject: `test ${numberOfMessagesSent}`,
        text: `just a test #${numberOfMessagesSent}`,
        html: `<p><h1>just a test</h1><br/>#${numberOfMessagesSent}</p>`,
      };
      const messageBody = JSON.stringify(message);
      const sendMessageParams = {
        MessageBody: messageBody,
        QueueUrl: queueUrl,
      };
      sqs
        .sendMessage(sendMessageParams)
        .promise()
        .then(() => {})
        .catch((err: Error) => {
          console.error(`SQS sendMessage failed ${numberOfMessagesSent}`, err);
        });
    }
    console.info(`Sent ${numberOfMessagesSent} messages.`);
  });

program.parse(process.argv);
