"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.main = exports.saveMessageRecord = void 0;
const nodemailer = require("nodemailer");
const aws_sdk_1 = require("aws-sdk");
const messageTableName = process.env.MESSAGE_TABLE_NAME ? process.env.MESSAGE_TABLE_NAME.trim() : '';
const sendingRate = process.env.SENDING_RATE ? Number.parseInt(process.env.SENDING_RATE, 10) : 5;
const maxConnections = process.env.MAX_CONNECTIONS ? Number.parseInt(process.env.MAX_CONNECTIONS, 10) : 5;
const booleanStrings = ['true', 'yes', 'y', '1'];
const debug = process.env.DEBUG ? booleanStrings.includes(process.env.DEBUG.toLowerCase()) : false;
const mockMailer = process.env.MOCK_MAILER ? booleanStrings.includes(process.env.MOCK_MAILER.toLowerCase()) : false;
const messageExpiryInDays = process.env.MESSAGE_EXPIRY_IN_DAYS ? Number.parseInt(process.env.MESSAGE_EXPIRY_IN_DAYS, 10) : 30;
const ses = new aws_sdk_1.SES({
    apiVersion: '2010-12-01',
});
const documentClient = new aws_sdk_1.DynamoDB.DocumentClient({
    maxRetries: 3,
    sslEnabled: true,
    apiVersion: '2012-08-10',
});
let transporter;
if (mockMailer) {
    console.log('enabling mock mailer');
    transporter = nodemailer.createTransport({
        jsonTransport: true,
    });
}
else {
    transporter = nodemailer.createTransport({
        SES: ses,
        sendingRate,
        maxConnections,
    });
}
exports.saveMessageRecord = (messageRecord) => {
    const dbParams = {
        TableName: messageTableName,
        Item: messageRecord,
    };
    return documentClient
        .put(dbParams)
        .promise()
        .then(() => true)
        .catch((err) => {
        console.error('db update failed', err);
        return false;
    });
};
exports.main = async (event) => {
    if (debug) {
        console.log(`received ${event.Records.length} records`);
    }
    const sendRequests = [];
    let numberOfSuccesses = 0;
    let numberOfFailures = 0;
    event.Records.forEach((record) => {
        const { body } = record;
        try {
            const message = JSON.parse(body);
            if (debug) {
                console.log(message);
            }
            sendRequests.push(transporter
                .sendMail(message)
                .then(async (response) => {
                if (messageTableName) {
                    const currentDate = new Date();
                    const expiryDate = new Date();
                    expiryDate.setDate(expiryDate.getDate() + messageExpiryInDays);
                    const expiry = Math.round(expiryDate.getTime() / 1000);
                    if (response && Object.prototype.hasOwnProperty.call(response, 'raw')) {
                        delete response.raw;
                    }
                    const messageRecord = {
                        id: record.md5OfBody,
                        message,
                        updateAt: currentDate.toISOString(),
                        expiry: expiry,
                        status: 'success',
                        response,
                    };
                    await exports.saveMessageRecord(messageRecord);
                }
                if (mockMailer) {
                    const pauseInMilliseconds = Math.round(Math.random() * 1000);
                    await new Promise((resolve) => setTimeout(resolve, pauseInMilliseconds));
                }
                return true;
            })
                .catch(async (err) => {
                console.error(`${message.subject} failed`);
                console.error(err);
                if (messageTableName) {
                    const { code, time, requestId, statusCode, retryable, retryDelay } = err;
                    const currentDate = new Date();
                    const expiryDate = new Date();
                    expiryDate.setDate(expiryDate.getDate() + messageExpiryInDays);
                    const expiry = Math.round(expiryDate.getTime() / 1000);
                    const messageRecord = {
                        id: record.md5OfBody,
                        message,
                        error: {
                            code,
                            time: time.toISOString(),
                            requestId,
                            statusCode,
                            retryable,
                            retryDelay,
                            message: err.message,
                        },
                        updateAt: currentDate.toISOString(),
                        expiry: expiry,
                        status: 'fail',
                    };
                    await exports.saveMessageRecord(messageRecord);
                }
                return false;
            }));
        }
        catch (err) {
            console.error('a fatal error occurred', err);
        }
    });
    await Promise.all(sendRequests)
        .then((responseStatuses) => {
        responseStatuses.forEach((wasSuccessful) => {
            wasSuccessful ? numberOfSuccesses++ : numberOfFailures++;
        });
        if (debug) {
            console.info(`number of successes = ${numberOfSuccesses}`);
            console.info(`number of failures = ${numberOfFailures}`);
        }
    })
        .catch((err) => {
        console.error('fatal error processing send request', err);
    });
};
//# sourceMappingURL=sqs-handler.js.map