"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.main = void 0;
const SQS = require("aws-sdk/clients/sqs");
const queueUrl = process.env.QUEUE_URL ? process.env.QUEUE_URL.trim() : '';
const booleanStrings = ['true', 'yes', 'y', '1'];
const debug = process.env.DEBUG ? booleanStrings.includes(process.env.DEBUG.toLowerCase()) : false;
const sqs = new SQS({
    apiVersion: '2012-11-05',
});
exports.main = async (event) => {
    let body = {};
    let response = {};
    let responseStatus = 'ERROR';
    if (!queueUrl) {
        const errorMessage = 'QUEUE_URL is not set';
        console.error(errorMessage);
        response = {
            message: errorMessage,
        };
    }
    else {
        try {
            body = JSON.parse(event.body);
            let idCounter = 0;
            if (body instanceof Array) {
                const entries = body.map((message) => {
                    idCounter++;
                    return {
                        Id: idCounter.toString(),
                        MessageBody: JSON.stringify(message, null),
                    };
                });
                if (debug) {
                    console.log(`received ${entries.length} messages`);
                }
                const sendMessageBatchParams = {
                    Entries: entries,
                    QueueUrl: queueUrl,
                };
                await sqs
                    .sendMessageBatch(sendMessageBatchParams)
                    .promise()
                    .then((data) => {
                    if (debug) {
                        console.log(data);
                    }
                    responseStatus = 'OK';
                })
                    .catch((error) => {
                    console.error(error);
                    response = {
                        error: error.message
                    };
                });
            }
            else {
                const messageBody = JSON.stringify(body, null);
                const sendMessageParams = {
                    MessageBody: messageBody,
                    QueueUrl: queueUrl,
                };
                if (debug) {
                    console.log('message received');
                }
                await sqs
                    .sendMessage(sendMessageParams)
                    .promise()
                    .then((data) => {
                    if (debug) {
                        console.log(data);
                    }
                    responseStatus = 'OK';
                    response = data;
                })
                    .catch((error) => {
                    console.error(error);
                    response = {
                        error: error.message
                    };
                });
            }
        }
        catch (error) {
            console.error('a fatal error occurred', error);
            response = {
                error: error.message
            };
        }
    }
    return {
        statusCode: 200,
        headers: {
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'OPTIONS,POST',
        },
        body: JSON.stringify({
            status: responseStatus,
            response,
        }),
    };
};
//# sourceMappingURL=api-handler.js.map