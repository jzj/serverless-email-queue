#  Serverless Email Queue

`Serverless Email Queue` is an application build using the [AWS Serverless Application Model](https://aws.amazon.com/serverless/sam/) (SAM) framework. SAM is an open-source framework for building serverless applications. `Serverless Email Queue` provides two interfaces for sending email messages.  An [Amazon Simple Queue Service](https://aws.amazon.com/sqs/) (SQS) message queue and an optional REST API. Email messages sent to the message queue are forwarded to the [Amazon Simple Email Service](https://aws.amazon.com/ses/) (SES).

> __Why not send messages directly to SES?__
> SES throttles API requests and will reject requests that exceed quota limits. `Serverless Email Queue` automatically throttles requests to SES.  Therefore, the rate for sending email messages to `Serverless Email Queue` can exceed the SES quota limits. `Serverless Email Queue` will store backlogged messages in a message queue until the sending capacity is available.

## Application Architecture

### Sequence Diagrams

![API Gateway Sequence](docs/images/http-rest-api-sequence.svg)

![SQS Sequence](docs/images/sqs-sequence.svg)

### AWS Infrastructure Diagram

![AWS Infrastructure](docs/images/serverless-email-queue-architecture-2d.png)

## Build and Deployment Instructions

These instructions are for a Linux operating system.  You can deploy this application from a Windows system; however, you will need to use the equivalent Windows commands:

1. Install the [SAM](https://aws.amazon.com/serverless/sam/) CLI.
2. Install [NodeJS](https://nodejs.org/en/download/). Release [12.20.0](https://nodejs.org/download/release/v12.20.0/) was used to build this project.  However, the latest recommended release should work.
3. Clone this repos.
4. In a terminal/console window, `cd` into the `serverless-email-queue` folder that you extracted.
5. In the base of the repo, run the following commands:

```bash
export AWS_SDK_LOAD_CONFIG=1
export SAM_CLI_TELEMETRY=0
npm install
npm run test
npm run build
sam build --base-dir ./dist
sam deploy --guided
```

## Usage

To send messages via the REST API, you need the following:

1. The URL of the REST API.
2. A valid [API Gateway](https://aws.amazon.com/api-gateway/) API key.

### AWS SQS API

To send messages directly to the message queue, you need the following:

1. The SQS queue URL.
2. Installation and configuration of the [AWS SDK](https://aws.amazon.com/tools/).
3. The AWS IAM permissions to publish to the SQS queue URL.

### REST API

### Sending Email Messages

The following are the possible fields of an email message:

* __from__ - The email address of the sender. All email addresses can be plain 'sender@example.com' or formatted '"Sender Name" sender@example.com'.
* __to__ - Comma separated list or an array of recipients email addresses that will appear on the To: field.
* __subject__ - The subject of the email.
* __text__ - The plaintext version of the message as an Unicode string.
* __html__ - The HTML version of the message as an Unicode string.

The format of the SQS message must be a JSON encoded string.  Here is an example:

```text
{\"from\":\"foo@example.com\",
\"to\":[\"success@simulator.amazonses.com\"],
\"subject\":\"test #1\",
\"text\":\"just a test\"}
```

#### Sending Email Messages via the REST API

#### A PHP Code Example

```php
$senderEmail = 'foo@example.com';
$recipientEmails = ['success@simulator.amazonses.com'];
$subject = 'Test Message from Serverless Email Gateway API';
$textBody = 'This email was sent with Serverless Email Gateway API.' ;
$htmlBody =  '<h1>Just a test</h1><p>This email was sent with Serverless Email Gateway API.</p>';
$apiUrl = 'https://0000000000.execute-api.us-east-1.amazonaws.com/Prod/send';
$apiKey = '0000000000000000000000000000000000000000';

foreach ($recipientEmails as $recipientEmail) {
  try {
    $message = array(
      'from' => $senderEmail,
      'to' => [$recipientEmail],
      'text' => $textBody,
      'html' => $htmlBody,
      'subject' => $subject
    );
    $messageBody = json_encode($message);
    $opts = array('http' =>
      array(
          'method'  => 'POST',
          'header'  => array('Content-Type: application/json', 'x-api-key: '.$apiKey),
          'content' => $messageBody
      )
    );
    $context = stream_context_create($opts);
    $result = file_get_contents($apiUrl, false, $context);
    if (!$result) {
      echo("Email queue via the API failed!\n".$result."\n");
      throw new ErrorException('unknown error');
    }
    echo("Email queued via the API!\n".$result."\n");
  } catch (Exception $e) {
    echo("The email was not sent via the API.\nError message: ".$e->getMessage()."\n");
    echo "\n";
  }
}
```

#### Sending Email Messages via the SQS Queue

#### A PHP Code Example

```php
require 'vendor/autoload.php';

use Aws\Sqs\SqsClient;
use Aws\Exception\AwsException;

$client = SqsClient::factory(array(
  'profile' => 'default',
  'region'  => 'us-east-1',
  'version' => '2012-11-05'
));

$queueUrl = 'https://sqs.us-east-1.amazonaws.com/00000000000/EmailMessageQueue';
$senderEmail = 'foo@example.com';
$recipientEmails = ['success@simulator.amazonses.com'];
$subject = 'Test Message from Serverless Email Gateway';
$textBody = 'This email was sent with Serverless Email Gateway SQS.' ;
$htmlBody =  '<h1>Just a test</h1><p>This email was sent with Serverless Email Gateway SQS</p>';

foreach ($recipientEmails as $recipientEmail) {
  try {
    $message = array(
      'from' => $senderEmail,
      'to' => [$recipientEmail],
      'text' => $textBody,
      'html' => $htmlBody,
      'subject' => $subject
    );
    $messageBody = json_encode($message);
    $params = [
      'MessageBody' => $messageBody,
      'QueueUrl' => $queueUrl
    ];
    $result = $client->sendMessage($params);
    echo("Email queued!\n".$result."\n");
  } catch (AwsException $e) {
    echo $e->getMessage();
    echo("The email was not sent to the queue.\nError message: ".$e->getAwsErrorMessage()."\n");
    echo "\n";
  }
}
```

## SES Send Results

`Serverless Email Queue` stores results of the SES requests in a DynamoDB table. These results are useful for troubleshooting failed messages. By default, the name of the DynamoDB table is `EmailQueueMessages`. To change the name of the table, change the `TableName` attribute's value in the `template.yml` file.

```yaml
  MessageDatabase:
    Type: AWS::DynamoDB::Table
    Properties:
      AttributeDefinitions:
        - AttributeName: id
          AttributeType: S
      KeySchema:
        - AttributeName: id
          KeyType: HASH
      BillingMode: PAY_PER_REQUEST
      TableName: <change me>
      TimeToLiveSpecification:
        AttributeName: expiry
        Enabled: true
```

Each entry in the DynamoDB table automatically expires after 30 days.  To change the days until expiration, update the `MESSAGE_EXPIRY_IN_DAYS` attribute in the `template.yml` file.

```yaml
  Environment:
    Variables:
      MESSAGE_TABLE_NAME: !Ref MessageDatabase
      DEBUG: false
      MOCK_MAILER: false
      MESSAGE_EXPIRY_IN_DAYS: <change me>
      AWS_NODEJS_CONNECTION_REUSE_ENABLED: 1
```
